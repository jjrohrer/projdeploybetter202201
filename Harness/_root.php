<?php
declare(strict_types=1);
require_once (__DIR__.'/../vendor/autoload.php');
require_once (__DIR__.'/WpEngine/DeploymentHelper.php');
require_once (__DIR__.'/WpEngine/WpApiBackups.php');

if (! isset($_ENV['WP_ENGINE_API_USERNAME'])) {
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
    $dotenv->load();
}