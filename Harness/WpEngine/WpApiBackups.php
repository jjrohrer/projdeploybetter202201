<?php
namespace WpEngine;
class WpApiBackups {
    CONST SNUM_STATUS_REQUESTED = 'requested';
    CONST SNUM_STATUS_INITIATED = 'initiated';
    CONST SNUM_STATUS_DONE = 'completed';
    CONST KNOWN_STATUS_OPTIONS = [self::SNUM_STATUS_REQUESTED, self::SNUM_STATUS_INITIATED, self::SNUM_STATUS_DONE];
    CONST STANDARD_SLEEP_TIME = 5;
    CONST MAX_PING_TRIES = 10;
    private static function GetStatusOfBackupOfId(string $installation_id, string $backupId): string {
        $asrStatus = DeploymentHelper::GetAsr("installs/".DeploymentHelper::GetInstallId_DEV()."/backups/{$backupId}");
        /*
         * (
            [status] => initiated
            [id] => 3aa6b917-e2b1-4f61-a2f5-a131c32cda63
        )
         */
        $snum_status = $asrStatus['status'];
        return $snum_status;
    }

    public static function StartBackupAndWaitUntilDone(string $installation_id): ?string {
        $asrRet = DeploymentHelper::PostSomething("installs/".DeploymentHelper::GetInstallId_DEV().'/backups', [
            'description'=> 'test backup 1'.uniqid(),
            'notification_emails' => ['gavin.wpengine@rohrer.org','jj.wpengine@rohrer.org']
        ]);
        $enum_status_requested_initiated = $asrRet['status'];
        $backup_id = $asrRet['id'];
        print("status: ".$enum_status_requested_initiated);
        if ($enum_status_requested_initiated != self::SNUM_STATUS_REQUESTED) {
            throw (new \Exception("OMG - WHAT DOES THAT MEAN: ".$enum_status_requested_initiated));
        }


        $count = 0;
        while ( ($count < self::MAX_PING_TRIES)) {
            $count = $count + 1;
            print "\nzzz ";
            sleep(self::STANDARD_SLEEP_TIME);
            $snum_status = static::GetStatusOfBackupOfId($installation_id, $backup_id);
            echo "\nstatus: $snum_status";
            if ($snum_status == self::SNUM_STATUS_DONE) {
                echo "\nDone backed up";
                return $backup_id;
            }
            if (! in_array($snum_status, self::KNOWN_STATUS_OPTIONS)) {
                throw (new \Exception("OMG - WHAT DOES THAT MEAN: ".$snum_status));
            }
        }
        return null;
    }
}
