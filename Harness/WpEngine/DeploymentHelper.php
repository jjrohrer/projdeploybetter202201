<?php
namespace WpEngine;

class DeploymentHelper
{
    public static function Hw(): void {
        print "Hello world.
        ";
    }
    private const URL_BASE = 'https://api.wpengineapi.com';
    private const APIR_VERSION_INDICATOR = 'v1';

    private static function BuildUrl(string $base, string $version, string $urlStub): string
    {
        return "{$base}/{$version}/{$urlStub}";
    }

    private static function Get_API_USERNAME(): string
    {
        return $_ENV['WP_ENGINE_API_USERNAME'];
    }

    private static function Get_API_PASSWORD(): string
    {
        return $_ENV['WP_ENGINE_API_PASSWORD'];
    }

    public static function Get_Site_ID(): string
    {
        return 'some site it.';
    }

    public static function GetAsr(string $urlStub): ?array
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::BuildUrl(self::URL_BASE, self::APIR_VERSION_INDICATOR, $urlStub));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        $headers = array();
        $cred_string = self::Get_API_USERNAME() . ":" . self::Get_API_PASSWORD();
        $headers[] = "Authorization: Basic " . base64_encode($cred_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $resultJson = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
            return null;
        }

        $asr = json_decode($resultJson, true);
        curl_close($ch);
        return $asr;
    }



    public static function PostSomething(string $urlStub, array $payload): ?array {



        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::BuildUrl(self::URL_BASE, self::APIR_VERSION_INDICATOR, $urlStub));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

        // ---- BE POSTING -BEGIN-
        $payload_json = json_encode($payload);
        //https://tecadmin.net/post-json-data-php-curl/
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload_json);

        // Set HTTP Header for POST request
        $cred_string = self::Get_API_USERNAME() . ":" . self::Get_API_PASSWORD();
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($payload_json),
                "Authorization: Basic " . base64_encode($cred_string)
            ]
        );

        $resultJson = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
            return null;
        }

        $asr = json_decode($resultJson, true);
        curl_close($ch);
        return $asr;

    }

    public static function GetInstallId_DEV() {
        return $_ENV['DEV_SITE_ID'];
    }
    public static function GetInstallId_STAGING() {
        return $_ENV['STAGING_SITE_ID'];
    }

}


