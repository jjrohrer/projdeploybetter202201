Tasks
=====
[X] Get code to run on pipes
    [X] Code needs to access the bit-bucket env vars since .env isn't there

[ ] See old stuff: Get code to rsync to the server (need ssh username - see the commented out section of bitbucket-pipelines.yml)

[ ] Run smoke test
        [ ] Simple walk through.
        [ ] Touch all known sites
        [ ] Make new site
            [ ] Aggressively walk through

[ ] Roll back if deploy doesn't work

[ ] See old stuff: Deploy to right installation

[ ] Don't deploy onto server with active users
    [ ] Count active users
    [ ] Wait until zero
    [ ] Go
    [ ] Maybe notify users
    [ ] Maybe drain incoming users
    [ ] Maybe kick out users


[ ] Have a warm copy we can muck with w/o touching prod






( ) Get PSR-4 autoloading working 