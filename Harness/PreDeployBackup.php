<?php
declare(strict_types=1);

require_once (__DIR__.'/_root.php');


$backup_id_elseNullIfFailed = \WpEngine\WpApiBackups::StartBackupAndWaitUntilDone(\WpEngine\DeploymentHelper::GetInstallId_DEV());
if (is_null($backup_id_elseNullIfFailed)) {
    throw (new \Exception("OMG - GOT NULL BACK "));
}
